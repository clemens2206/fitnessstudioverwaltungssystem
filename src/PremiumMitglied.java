/**
 * Die PremiumMember-Klasse ist eine Subklasse von Mitglied und soll von Premium-Mitglied des Fitnessstudios verwendet werden.
 *
 * @author Clemens Kerber
 * @version 1.0(16.05.2021)
 */

public class PremiumMitglied extends Mitglied{

    /**
     * Constructor von der Klasse PremiumMitglied.
     *
     * @param name Name des Mitglieds
     * @param email E-Mail des Mitglieds
     * @param address Adresse des Mitglieds
     * @param geschlecht Geschlecht des Mitglieds
     * @param hoehe Größe(Höhe) des Mitglieds
     * @param startGewicht Startgewicht des Mitglieds
     * @param gewaehlesPacket Gewähltes Packet des Mitglieds
     */
    public PremiumMitglied(String name, String email, String address, String geschlecht, float hoehe, float startGewicht, String gewaehlesPacket) {
        super(name, email, address, geschlecht, hoehe, startGewicht, gewaehlesPacket);
    }

    /**
     * Methode, die das vom Premium-Mitglied gewählte Paket festlegt.
     *
     * @param gewaehltesPacket Packetwahl
     */
    public void gewaehltesPacket(String gewaehltesPacket){
        setGewaehltesPacket(gewaehltesPacket);
    }

    /**
     * toString-Methode, die die Details eines Premium-Mitglieds als formatierten String zurückgibt.
     *
     * @return formatierter String mit den Details des Mitglieds.
     */
    public String toString(){
        String str = super.toString();
        return str;
    }
}
