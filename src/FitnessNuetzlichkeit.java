/**
 * Die Klasse FitnessNuetzlich wird verwendet, um statische Methoden zu speichern, die
 * von anderen Anwendungen verwendet werden können.
 *
 * @author Clemens Kerber
 * @version 1.0(28.05.2021)
 */

public class FitnessNuetzlichkeit {

    /**
     * Eine Methode, die verwendet wird, um den BMI eines Mitglieds zu berechnen.
     * Die zurückgegebene Zahl wird auf zwei Dezimalstellen gerundet.
     *
     * @param mitglieder Mitglied, für das der BMI berechnet wird
     * @param bewertung Letzte Bewertung für das Mitglied
     * @return bmiGerundet double-Wert
     */
        public static double kalkuliereBMI(Mitglied mitglieder, Bewertung bewertung){
            float gewicht;
            if(bewertung == null){
                gewicht = mitglieder.getStartGewicht();
            }else{
                gewicht = bewertung.getGewicht();
            }
            double bmi = gewicht / (mitglieder.getHoehe() * mitglieder.getHoehe());
            double bmiGerundet = Math.round(bmi * 100) /100.0;
            return bmiGerundet;
        }

    /**
     * Eine Methode, die das BMI-Ergebnis eines Mitglieds kategorisiert
     *
     * @param bmiValue berücksichtigt den BMI-Wert des Mitglieds
     * @return gibt die BMI-Kategorie als String zurück.
     */
    public static String bestimmenDerBmiKategorie(double bmiValue) {
            String str = "";
            if (bmiValue < 16.0) {
                str = "Starkes Untergewicht";
            } else if (bmiValue >= 16.0 && bmiValue < 18.5) {
                str = "Untergewicht";
            } else if (bmiValue >= 18.5 && bmiValue < 25.0) {
                str = "Normal";
            } else if (bmiValue >= 25.0 && bmiValue < 30.0) {
                str = "Übergewicht";
            } else if (bmiValue >= 30.0 && bmiValue < 35.0){
                str = "Starkes Übergewicht";
            }else if(bmiValue >= 35.0){
                str = "Adipositas";
            }
            return str;
        }

    /**
     * Methode zur Umrechnung des Kilogrammwerts in Pfund
     *
     * @param kg Wert in Kilogramm
     * @return cGerundet pounds
     */
    public static double kgUmrechnenInPounds(double kg){
            double x = kg*2.20462;
            double xGerundet = Math.round(x*100.0)/100.0;
            return xGerundet;
        }
    }


